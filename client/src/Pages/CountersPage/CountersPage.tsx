import React, { Component, Dispatch } from 'react';
import { connect } from 'react-redux';
import {
  addCounter,
  removeCounter,
  incrementCounter,
  decrementCounter,
} from '../../store/actions/countersActionCreators';

function mapStateToProps(state: RootState) {
  const { counters } = state.counter;
  return { counters };
}

function mapDispatchToProps(dispatch: Dispatch<RootAction>) {
  return {
    addCounter: () => dispatch(addCounter()),
    removeCounter: (id: number) => dispatch(removeCounter(id)),
    incrementCounter: (id: number) => dispatch(incrementCounter(id)),
    decrementCounter: (id: number) => dispatch(decrementCounter(id)),
  };
}

interface DispatchProps {
  addCounter: () => void;
  removeCounter: (id: number) => void;
  incrementCounter: (id: number) => void;
  decrementCounter: (id: number) => void;
}

interface CountersPageProps {
  counters: ICounter[];
}

@(connect(mapStateToProps, mapDispatchToProps) as any)
export default class CountersPage extends Component<DispatchProps & CountersPageProps> {
  async componentDidMount() {}

  render() {
    const { counters, addCounter, removeCounter, incrementCounter, decrementCounter } = this.props;

    return (
      <div className="CountersPage">
        {counters.map((counter, index) => (
          <div key={index}>
            <h1>Counter {counter.id}</h1>
            <h5>Counter {counter.value}</h5>
            <button onClick={() => decrementCounter(counter.id)}>
              Decrementar counter {counter.id}
            </button>
            <button onClick={() => incrementCounter(counter.id)}>
              Incrementar counter {counter.id}
            </button>
            <button onClick={() => removeCounter(counter.id)}>Eliminar counter {counter.id}</button>
          </div>
        ))}

        <button onClick={() => addCounter()}>Add counter</button>
      </div>
    );
  }
}
