import React, { Component, Key } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { preloadPlaylist } from '../../store/actions/playlistActionCreators';

function mapStateToProps(state: RootState) {
  const { playlist, color, isDataLoaded } = state.playlist;
  return { playlist, color, isDataLoaded };
}

function mapDispatchToProps(dispatch: ThunkDispatch<RootState, {}, RootAction>) {
  return {
    preloadPlaylist: () => dispatch(preloadPlaylist()),
  };
}

interface DispatchProps {
  preloadPlaylist: () => Promise<void>;
}

interface PlaylistPageProps {
  playlist: IPlaylistItem[];
  color: any;
  isDataLoaded: boolean;
}

@(connect(mapStateToProps, mapDispatchToProps) as any)
export default class PlaylistPage extends Component<DispatchProps & PlaylistPageProps> {
  async componentDidMount() {
    let { preloadPlaylist } = this.props;
    await preloadPlaylist();
  }

  render() {
    const { playlist, isDataLoaded } = this.props;

    return (
      <div className="Home">
        {isDataLoaded ? (
          <div>
            {playlist.map((item: IPlaylistItem, index: Key) => (
              <div className={`item-${index}`} key={index}>
                <h1>{item.title.original}</h1>
              </div>
            ))}
          </div>
        ) : (
          <h1>Cargando...</h1>
        )}
      </div>
    );
  }
}
