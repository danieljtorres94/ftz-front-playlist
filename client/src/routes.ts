import PlaylistPage from './Pages/PlaylistPage/PlaylistPage';
import CountersPage from './Pages/CountersPage/CountersPage';

const routes: AppRoute[] = [
  {
    title: 'Contenidos',
    path: '/contenidos',
    component: PlaylistPage,
    exact: true,
  },
  {
    title: 'Contadores',
    path: '/contadores',
    component: CountersPage,
    exact: true,
  },
];

export default routes;
