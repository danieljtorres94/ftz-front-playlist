import get from 'axios';

export const getPlaylist = async () => {
  const {
    data: {
      data: { color, title, items: playlist },
    },
  } = await get('/api/contenidos');

  return { color, title, items: playlist };
};
