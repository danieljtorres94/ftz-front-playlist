import * as actionTypes from '../actionTypes';

export const addCounter = (): RootAction => {
  console.log('addCounter');
  return {
    type: actionTypes.ADD_COUNTER,
    payload: null,
  };
};

export const removeCounter = (id: number): RootAction => {
  console.log('removeCounter', id);
  return {
    type: actionTypes.REMOVE_COUNTER,
    payload: id,
  };
};

export const incrementCounter = (id: number): RootAction => {
  console.log('incrementCounter', id);
  return {
    type: actionTypes.INCREMENT_COUNTER,
    payload: id,
  };
};

export const decrementCounter = (id: number): RootAction => {
  console.log('decrementCounter', id);
  return {
    type: actionTypes.DECREMENT_COUNTER,
    payload: id,
  };
};
