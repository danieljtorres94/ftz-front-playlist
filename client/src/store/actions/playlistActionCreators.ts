import * as actionTypes from '../actionTypes';
import { ThunkAction } from 'redux-thunk';
import { getPlaylist } from '../../service';

export const preloadPlaylist = (): ThunkAction<void, RootState, unknown, RootAction> => {
  return async (dispatch: DispatchType) => {
    try {
      const { color, title, items: playlist }: any = await getPlaylist();

      const action: RootAction = {
        type: actionTypes.PRELOAD_PLAYLIST,
        payload: {
          isDataLoaded: true,
          color,
          title,
          playlist,
        },
      };

      dispatch(action);
    } catch (error) {
      console.log(error);
    }
  };
};
