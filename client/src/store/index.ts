import { createStore, applyMiddleware, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers';

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));

const store: Store<RootState, RootAction> & {
  dispatch: DispatchType;
} = createStore(reducers, composedEnhancer);

export default store;
