import * as actionTypes from '../actionTypes';

const initialState: CountersState = {
  counters: [],
};

const reducer = (state: CountersState = initialState, action: RootAction): CountersState => {
  console.log(action);
  switch (action.type) {
    case actionTypes.ADD_COUNTER: {
      const newCounter: ICounter = {
        id: state.counters.length ? state.counters[state.counters.length - 1].id + 1 : 1,
        value: 0,
      };

      return {
        ...state,
        counters: [...state.counters, newCounter],
      };
    }
    case actionTypes.REMOVE_COUNTER: {
      return {
        ...state,
        counters: [...state.counters.filter((counter) => counter.id != action.payload)],
      };
    }
    case actionTypes.INCREMENT_COUNTER: {
      const index = state.counters.findIndex((counter) => counter.id === action.payload);

      console.log(index);

      const newArray = [...state.counters];

      if (index !== undefined) newArray[index].value++;

      return {
        ...state,
        counters: newArray,
      };
    }
    case actionTypes.DECREMENT_COUNTER: {
      const index = state.counters.findIndex((counter) => counter.id === action.payload);

      const newArray = [...state.counters];

      if (index !== undefined) newArray[index].value--;

      return {
        ...state,
        counters: newArray,
      };
    }
  }
  return state;
};

export default reducer;
