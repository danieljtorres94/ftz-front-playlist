import { combineReducers } from 'redux';
import counter from './countersReducer';
import playlist from './playlistReducer';

export default combineReducers({
  counter,
  playlist,
});
