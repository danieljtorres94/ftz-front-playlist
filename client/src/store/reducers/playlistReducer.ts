import * as actionTypes from '../actionTypes';

const initialState: PlaylistState = {
  isDataLoaded: false,
  color: '',
  title: {
    original: '',
    es: '',
  },
  playlist: [],
};

const reducer = (state: PlaylistState = initialState, action: RootAction): PlaylistState => {
  switch (action.type) {
    case actionTypes.PRELOAD_PLAYLIST:
      return {
        ...state,
        ...action.payload,
      };
  }
  return state;
};

export default reducer;
