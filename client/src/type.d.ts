type RootAction = {
  type: String;
  payload: any;
};

type DispatchType = (args: RootAction) => RootAction;

type AppRoute = {
  title: string;
  path: string;
  component: any;
  exact: boolean;
};

type RootState = {
  playlist: PlaylistState;
  counter: CountersState;
};

type PlaylistState = {
  isDataLoaded: boolean;
  color: string;
  title: ITitle;
  playlist: IPlaylistItem[];
};

interface ITitle {
  original: string;
  es?: string;
}

interface IPlaylistItem {
  _id: string;
  title: ITitle;
}

type CountersState = {
  counters: ICounter[];
};

interface ICounter {
  id: number;
  value: number;
}
