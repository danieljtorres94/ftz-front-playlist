const fastify = require("fastify")({ logger: true });
const get = require("axios").get;

process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const NUNCHEE_URI = "https://mychannel.nunchee.tv";

fastify.get("/api", async (request, reply) => {
  return { hello: "world" };
});

fastify.get("/api/contenidos", async (request, reply) => {
  try {
    const {
      data,
    } = await get(
      `${NUNCHEE_URI}/api/generic/playlists/details/5b845b8346cc29000e4f186a`,
      { data: { itemsPerPage: 10 } }
    );

    reply.send(data);
  } catch (error) {
    console.log(error);
    reply.status(500).send(error.message);
  }
});

const start = async () => {
  try {
    await fastify.listen(3030, "0.0.0.0");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
